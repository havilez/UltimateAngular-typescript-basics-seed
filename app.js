"use strict";
// Class is syntactic sugar to create an Object with construtor and prototypal inheritance
var Pizza = /** @class */ (function () {
    function Pizza(name) {
        this.toppings = [];
        this.name = name;
    }
    Pizza.prototype.addTopping = function (topping) {
        this.toppings.push(topping);
    };
    return Pizza;
}());
// function Pizza( name :string) {
//     this.name = name;
//     this.toppings = [];
// }
// Pizza.prototype.addTopping = function addTopping ( topping :string) {
//     this.toppings.push(topping);
// }
var pizza = new Pizza('Pepperoni');
pizza.addTopping('pepperoni');
