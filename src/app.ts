
// virtual overload function declaration example

function reverse(str :string) :string;
function reverse<T>(arr :T[]) :T[];
function reverse<T>( stringOrArray :string|T[]) :string | T[] {
    if (typeof stringOrArray === 'string') {
        return  stringOrArray
                    .split('')
                    .reverse()
                    .join('');
    }
    // clone the array then reverse it, since array is passed by reference
    return( stringOrArray.slice().reverse() );
};

console.log(reverse('Pepperoni') );
console.log( reverse(['bacon', 'pepperoni', 'chili', 'mushrooms']) );
console.log( reverse([1,2,3,4]) );